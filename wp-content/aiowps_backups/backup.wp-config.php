<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'gran_site' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'gran_root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'r@hc903@!' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Ta-k4/}Iw90UKx.wauZV]~}&6OQh7C.T#wHH<|:t;XS1w[=NHV0^vI!*sj?Z%<ya' );
define( 'SECURE_AUTH_KEY',  'Y}y6_Y@m)@Iv6kIX9r`hhps8%[,[;Kjhvp6Z|+w.Es0}Kij5^4RVdBQQSvihPL:g' );
define( 'LOGGED_IN_KEY',    ',h*72rA,+yauul~e@wW5~Z5.<h(%Wmg:|rku3Sl4[}xn^BWK7TtF|^)D90ScQ^~s' );
define( 'NONCE_KEY',        '>@BJsL-jOA1o]WaU:ioZ-,z7CrE9KTou(bB5o_h4k(N0w,=4W-4Ay~o;kp(fVPSz' );
define( 'AUTH_SALT',        'Lz7~59+=LBM;`phn+.|BEppnLAcG%uFlvpHIz5u!x++,asq#NJFC1NT`&lRH7I6?' );
define( 'SECURE_AUTH_SALT', '{SCHK*V1#O OA(t60b.MgUEoUnI%<)wp9+o= *$B<`$H@+*|E1YB^??T9NJ^MVYg' );
define( 'LOGGED_IN_SALT',   'JaAu:vy&AlsiBPdJ(mA5w0O-Np(0q!/l3UEj|t|l@eV0Q#Kt3c*t7DqHH_v?|42^' );
define( 'NONCE_SALT',       'B{3/`7ab,A)aBD|>@jG:~=?Gy5]4uLT0Y9wt<x7yz)IUCEb{=og|CKk5oy1VL:^=' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'gr_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
