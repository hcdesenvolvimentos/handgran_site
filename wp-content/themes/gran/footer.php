<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gran
 */
	global $configuracao;
?>
<footer>
	<div class="hand-containerLarge">
		<nav>
			<ul>
				
				<li class="footer-menu">
					<?php 
					if (trim($configuracao['gran_gran'])):
						echo "<strong>".$configuracao['gran_gran']."</strong>";
						wp_nav_menu( array('menu' => $configuracao['gran_gran'],));
					endif;
					?>
				</li>
				<li class="footer-menu">
					<?php 
					if (trim($configuracao['gran_solucao'])):
						echo "<strong>".$configuracao['gran_solucao']."</strong>";
						wp_nav_menu( array('menu' => $configuracao['gran_solucao'],));
					endif;
					?>
				</li>
				<li class="footer-menu">
					<?php if (trim($configuracao['gran_contato'])): ?>
					<strong><?php echo $configuracao['gran_contato'] ?></strong>
					<?php if (trim($configuracao['gran_endereco'])): ?>
					<a target="_blank" href="https://www.google.com.br/maps/place/<?php echo $configuracao['gran_endereco'] ?>"><?php echo $configuracao['gran_endereco'] ?></a>
					<?php endif; if (trim($configuracao['gran_telefone'])): ?>
					<a target="_blank" href="tel:<?php echo $configuracao['gran_telefone'] ?>"><?php echo $configuracao['gran_telefone'] ?></a>
					<?php endif; if (trim($configuracao['gran_email'])): ?>
					<a target="_blank" href="malito:<?php echo $configuracao['gran_email'] ?>"><?php echo $configuracao['gran_email'] ?></a>
					<?php endif; endif; ?>
				</li>
				<li class="redes-sociais-footer">
					<?php if (trim($configuracao['gran_contato_social'])): ?>
					<strong><?php echo $configuracao['gran_contato_social'] ?></strong>
					<div class="container-footer-item">
						<?php if (trim($configuracao['gran_instagram'])): ?>
						<a target="_blank" href="<?php echo $configuracao['gran_instagram'] ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/instagram-logo.svg">
						</a>
						<?php endif; if (trim($configuracao['gran_facebook'])): ?>
						<a target="_blank" href="<?php echo $configuracao['gran_facebook'] ?>" class="text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/img/facebook-logo.svg">
						</a>
						<?php endif; if (trim($configuracao['gran_twitter'])): ?>
						<a target="_blank" href="<?php echo $configuracao['gran_twitter'] ?>" class="text-right" >
							<img src="<?php echo get_template_directory_uri(); ?>/img/twitter-logo.svg">
						</a>
						<?php endif; if (trim($configuracao['gran_linkedin'])): ?>
						<a target="_blank" href="<?php echo $configuracao['gran_linkedin'] ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/linkedin-logo.svg">
						</a>
						<?php endif; if (trim($configuracao['gran_behance'])): ?>
						<a target="_blank" href="<?php echo $configuracao['gran_behance'] ?>" class="text-center">
							<img src="<?php echo get_template_directory_uri(); ?>/img/behance-logo.svg">
						</a>
						<?php endif; if (trim($configuracao['gran_dribbble'])): ?>
						<a target="_blank" href="<?php echo $configuracao['gran_dribbble'] ?>" class="text-right">
							<img src="<?php echo get_template_directory_uri(); ?>/img/dribbble-logo.svg">
						</a>
					<?php endif;endif;  ?>
					</div>
				</li>
				<li class="formulario-footer">
					<strong>Painel do cliente</strong>
					<div class="form-footer">
						<input type="text" placeholder="E-mail">
						<input type="password" placeholder="Senha">
						<div class="text-right">	
							<input type="submit" name="entrar" value="entrar">
						</div>
					</div>
				</li>
			</ul>
		</nav>

		<div class="copyright">
			<?php  if (trim($configuracao['gran_header_copyright'])): ?>
			<p><?php echo $configuracao['gran_header_copyright']; ?></p>
			<?php endif; if (trim($configuracao['gran_header_logo_footer']['url'])): ?>
			<figure>
				<img src="<?php echo $configuracao['gran_header_logo_footer']['url']; ?>" alt="<?php echo bloginfo(); ?>">
			</figure>
			<?php endif; ?>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>

</body>
</html>
