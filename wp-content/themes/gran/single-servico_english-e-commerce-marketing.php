<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gran
 */

global $post;

//RECUPERANDO NOME TAXONOMIA 
$terms = get_the_terms( $post->ID, 'categoriaservicos_english');
//TÍTULO POST ATUAL
$post_title = $post->post_title;

//CLASSE ATIVO
$ativo_item = "ativo-item";

//QUERY CUSTON POST TYPE/ RECUPERANDO SERVIÇOS
$servicos = new WP_Query(
					array(
						'post_type'     => 'servico_english',
						'ordeby'        => 'id',
						'order'         => 'asc',
						'post_per_page' => 1,
						'tax_query'     => array(
						array(
							'taxonomy' => 'categoriaservicos_english',
							'field'    => 'slug',
							'terms'    => $terms[0]->slug ,
							)
						)

					)
				);
get_header();

// TEMPLATE E-commerce Marketing -> hand-prod-mod-1
include (TEMPLATEPATH . '/templates-english/hand-prod-mod-english-1.php');

get_footer();
