<div class="pg hand-prod-mod-5">
			
	<section class="hand-destaque-prod">
		<img src="img/grid2.svg">
		<div class="hand-containerLarge">
			<article>
				<span class="hand-subTitle-topo"><?php echo $terms[0]->name; ?></span>
				<h1 class="hand-title-destaque"><?php echo get_the_title(); ?></h1>
				<p class="hand-paragrafo-deatque"><?php echo rwmb_meta('Gran_services_english_subtitulo_commerce_Marketing'); ?></p>
			</article>

			<nav>
				<ul id="carrosselMenuItem" class="owl-Carousel">
					<?php while($servicos->have_posts()) : $servicos->the_post(); ?>
					<li class="item  <?php if($post_title == get_the_title()){ echo $ativo_item;}; ?> ">
						<a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
					</li>
					<?php endwhile;wp_reset_query(); ?>
				</ul>
			</nav>
		</div>
	</section>

	<section class="hand-naming">
		
		<div class="row">
			<div class="col-md-6">
				<figure class="template-desktop">
					<img src="<?php echo rwmb_meta('services_english_design_naming_Imagem_ilustrativa_texto')['full_url']; ?>" alt="<?php echo rwmb_meta('services_english_design_naming_Imagem_ilustrativa_texto')['full_url']; ?>">
				</figure>
			</div>
			<div class="col-md-6">
				<article class="formatacaoLista">
					<?php echo rwmb_meta('Gran_services_english_design_naming_texto'); ?>
				</article>
				<figure class="template-mobile">
					<img src="<?php echo rwmb_meta('services_english_design_naming_Imagem_ilustrativa_texto')['full_url']; ?>" alt="<?php echo rwmb_meta('services_english_design_naming_Imagem_ilustrativa_texto')['full_url']; ?>">
				</figure>
			</div>
		</div>

	</section>

	<div class="hand-containerFull">
		<section class="hand-naming-processo">
			<div class="row">
				<div class="col-md-5">
					<div class="hand-naming-processo-titulo">
						<img src="<?php echo rwmb_meta('services_english_design_naming_Imagem_ilustrativa_box')['full_url']; ?>" alt="<?php echo rwmb_meta('services_english_design_naming_Imagem_ilustrativa_box')['full_url']; ?>">
						<h3><?php echo rwmb_meta('services_english_design_naming_titulo_box'); ?></h3>
					</div>
				</div>		
				<div class="col-md-6">
					<div class="hand-naming-processo-description">
						<p><?php echo rwmb_meta('services_english_design_naming_texto_box'); ?></p>
					</div>
				</div>
			</div>
		</section>
	</div>

	<section class="hand-naming-categoria">
		
		<div class="hand-naming-categoria-title">
			<span><?php echo rwmb_meta('services_english_design_naming_titulo_area_logo'); ?></span>
			<p><?php echo rwmb_meta('services_english_design_naming_texto_area_logo'); ?></p>

			<div class="hand-naming-area-categoria">
				<div class="row">
					<div class="col-xs-6">
						<div class="hand-naming-area-categoria-list formatacaoLista">
							<ul>
								<?php 

									$categoria = 'categorialogodesignnaming';
									// LISTA AS CATEGORIAS DE PRODUTO
									$categoriasProdutos = get_terms( $categoria, array(
										'orderby'    => 'count',
										'hide_empty' => true,
										'parent'	 => "",
										'order'      => 'DESC',
									));

									$i = 0;
									foreach($categoriasProdutos as $categoriasProdutos):
										$nome  = $categoriasProdutos->name;
										$slug = $categoriasProdutos->slug;
										if($i == 0):
											$ativo = "ativo";
									

								 ?>
								<li class="<?php echo $ativo; ?>" data-id="<?php echo $slug ?>"><?php echo $nome ?></li>

								<?php else: ?>
								<li  data-id="<?php echo $slug ?>"><?php echo $nome ?></li>
								<?php endif;$i++;endforeach; ?>
							</ul>
						</div>
					</div>
					<div class="col-xs-6">
						<?php 

							$categoria = 'categorialogodesignnaming';
							// LISTA AS CATEGORIAS DE PRODUTO
							$categoriasProdutos = get_terms( $categoria, array(
								'orderby'    => 'count',
								'hide_empty' => true,
								'parent'	 => "",
								'order'      => 'DESC',
							));

							foreach($categoriasProdutos as $categoriasProdutos):
								$slug = $categoriasProdutos->slug;
						 ?>
						<div class="hand-naming-area-categoria-list area-logo" id="<?php echo $slug ?>">
							<?php 
									//LOOP DE POST CATEGORIA DESTAQUE				
								$logos = new WP_Query(array(
									'post_type'     => 'logodesignnaming',
									'posts_per_page'   => -1,
									'tax_query'     => array(
										array(
											'taxonomy' => $categoria,
											'field'    => 'slug',
											'terms'    => $slug,
											)
										)
									)
								);

								if ($logos):
									// LOOP DE POST
									while ( $logos->have_posts() ) : $logos->the_post();
							?>
							<figure>
								<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title()?>">
							</figure>
							<?php endwhile;endif;  ?>
								
						</div>
						<?php endforeach;?>
					</div>
				</div>
			</div>
		</div>
		
	</section>

	<div class="hand-containerLarge">
		<section class="hand-info-prod">
			<span><?php echo rwmb_meta('Gran_services_english_design_naming_titulo_rodape'); ?></span>
			<h3><?php echo rwmb_meta('Gran_services_english_design_naming_texto_rodape'); ?></h3>
			<a class="" href="<?php echo rwmb_meta('Gran_services_english_design_naming_link_rodape'); ?>">Let´s do This!</a>
		</section>
	</div>

</div>	