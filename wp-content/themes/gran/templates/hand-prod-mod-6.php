<div class="pg pg-websites">
	<section class="hand-destaque-prod">
		<img src="<?php echo get_template_directory_uri(); ?>/img/grid-fundo.svg">
		<div class="hand-containerLarge">
			<article>
				<span class="hand-subTitle-topo"><?php echo $terms[0]->name; ?></span>
				<h1 class="hand-title-destaque"><?php echo get_the_title(); ?></h1>
				<p class="hand-paragrafo-deatque">Sites pensados para pessoas e para robôs</p>
			</article>

			<nav>
				<ul id="carrosselMenuItem" class="owl-carousel">
					<?php while($servicos->have_posts()) : $servicos->the_post(); ?>
					<li class="item  <?php if($post_title == get_the_title()){ echo $ativo_item;}; ?> ">
						<a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
					</li>
					<?php endwhile;wp_reset_query(); ?>
				</ul>
			</nav>
		</div>
	</section>

	<section class="hand-conteudo-prod">
		
		<article>
			<div class="hand-container">
				<div class="row">
					<div class="col-md-4">
						<div class="hand-sub-title">
							<h3 class="hand-title-section-home">Porque preciso <br> de um Website?</h3>
						</div>
					</div>
					<div class="col-md-8">
						<h4>Faça um teste: quando você vai a uma reunião ou lê algo sobre uma empresa, o que faz na sequência?</h4>
						<div class="row">
							<div class="col-sm-6">
								<div class="hand-texto-prod hand-formatacao-texto">
									<p>Sabe por que a sua empresa deve ter um web site? Porque ele se tornou a porta de entrada das empresas, o que apresenta a marca, os produtos e serviços para o consumidor. É pelo site que a maioria dos consumidores define se segue em frente na seleção de sua escolha.</p>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="hand-texto-prod formatacaoLista">
									<p>O sucesso de um site vai além do layout inteligente, que harmoniza com a marca. Ele precisa conhecer o seu público-alvo e disponibilizar ferramentas e prestação de serviços que sejam atrativos para a efetivação do negócio.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>

	</section>


	<section class="sessao-descricao-geral-website">
		<div class="hand-containerFull">
			<article>	
				<div class="row">
					<div class="col-md-6 text-right">
					
						<div class="sessao-descricao-geral-website-texto-left">
							<h2>A equipe da Handgran dá<br> algumas dicas para quem <br> busca ter um site que alie<br> design e assertividade:</h2>
							<ul>
								<li>Ele deve ser responsivo, ou seja, deve conversar com as<br> diferentes dispositivos além do computador tradicional: celular e<br> tablets, por exemplo.</li>
								<li>É muito importante que o seu site esteja linkado com as redes<br> sociais, porque é por lá que o seu cliente final poderá interagir e<br> saber todas as novidades da sua empresa.</li>
								<li>Seu site deve cumprir os requisitos de velocidade e tecnologia<br> que o google exige, é ele é equivalente as antigas páginas<br> amarelas.</li>
								<li>As informações devem ser atualizadas com frequência, de acordo<br> com o produto ou serviço que oferece. Portanto, o site deve ter<br> páginas administráveis, ou seja, de fácil atualização.</li>
							</ul>
						</div>

					</div>
					<div class="col-md-6">
						<div class="sessao-descricao-geral-website-texto-right">
							
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/img/Rectangle Copy 3@1,5x.svg" alt="">
							</figure>

							
							<div class="sessao-descricao-geral-website-texto-right-area-text">
								<p>O que podemos fazer:</p>

								<ul>
									<li>Sites Responsivos</li>
									<li>Atualização e Manutenção</li>
									<li>Sites Estáticos</li>
									<li>Fast Sites</li>
									<li>Landing Pages</li>
									<li>Hot Sites</li>
									<li>Sites Administráveis</li>
								</ul>
							</div>

						</div>	
					</div>
				</div>
			</article>
		<div class="hand-container">

	</section>

	<section class="sessao-metodologia-website">
		<h2>Metodologia</h2>

		<div class="sessao-metodologia-website-descricao">
			<div class="hand-container">
				<div class="row">
					<div class="col-sm-8">
						<div class="sessao-metodologia-website-descricao-titulo">
							<h3>Tornar seu site acessível para seus <br> usuários significa adequa-los à maneira <br> de como eles navegam.</h3>
							<p>Usamos o design e a arquitetura para estimular a navegação com foco em<br> resultados, seja para conversão de cliques ou no reforço da sua imagem.</p>
							
						</div>
					</div>
					<div class="col-sm-4">
						<div class="sessao-metodologia-website-descricao-texto">
							<h4>Projetos relacionados</h4>
							<ul>
								<li>Sumera</li>
								<li>Stardente</li>
								<li>Unika</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<figure>
			<img src="<?php echo get_template_directory_uri(); ?>/img/fluxo2.png" alt="">
		</figure>
	</section>

	<div class="hand-containerLarge">
		<section class="hand-info-prod">
			<span>Let’s talk</span>
			<h3>E ai? vamos  <br> surpreender seus<br> visitantes?🤯</h3>
			<a class="" href="#">Me Ajude a investir certo</a>
		</section>
	</div>
	
</div>