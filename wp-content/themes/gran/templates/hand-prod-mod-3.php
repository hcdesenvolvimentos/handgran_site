<div class="pg hand-prod-mod-3">
			
	<section class="hand-destaque-prod">
		<img src="<?php echo get_template_directory_uri(); ?>/img/grid-fundo.svg">
		<div class="hand-containerLarge">
			<article>
				<span class="hand-subTitle-topo"><?php echo $terms[0]->name; ?></span>
				<h1 class="hand-title-destaque"><?php echo get_the_title(); ?></h1>
				<p class="hand-paragrafo-deatque"><?php echo rwmb_meta('Gran_servico_subtitulo_commerce_Marketing'); ?></p>
			</article>

			<nav>
				<ul id="carrosselMenuItem" class="owl-carousel">
					<?php while($servicos->have_posts()) : $servicos->the_post(); ?>
					<li class="item  <?php if($post_title == get_the_title()){ echo $ativo_item;}; ?> ">
						<a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
					</li>
					<?php endwhile;wp_reset_query(); ?>
				</ul>
			</nav>
		</div>
	</section>

	<section class="hand-prod-mod-content-text">
		<div class="hand-containerLarge">
			<div class="row">
				<div class="col-sm-6">
					<div class="hand-prod-mod-content-text-area hand-formatacao-texto">
						<h4 class="hand-tile-responsive"><?php echo rwmb_meta('Gran_servico_paid_search_titulo_oque_e'); ?></h4>
						<?php echo rwmb_meta('Gran_servico_paid_search_texto_oque_e'); ?>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="hand-prod-mod-content-text-area formatacaoLista beneficios">
						<article class="formatacaoLista">	
							<span><?php echo rwmb_meta('Gran_servico_paid_search_texto_direito'); ?></span>
							<?php 
								$logosMarca = rwmb_meta('Gran_servico_paid_search_logo_direito');
								foreach($logosMarca as $logosMarca):
									$logoMarca = $logosMarca['full_url'];				

							?>
							<img src="<?php echo $logoMarca ?>" alt="<?php echo $logoMarca ?>">
							<?php endforeach;?>

							<span><?php echo rwmb_meta('Gran_servico_paid_search_titulo_beneficios_direito') ?></span>
							<ul>
							<?php 
								$lista_beneficio = rwmb_meta('Gran_servico_paid_search_lista_beneficios_direito');
								foreach($lista_beneficio as $lista_beneficio):
							?>
							<li><?php echo $lista_beneficio ?></li>
							<?php endforeach;?>
							</ul>
						</article>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="hand-prod-mod-content-como-funciona">
		<div class="row">
			<div class="col-md-6">
				<div class="hand-prod-mod-content-text-area-como-funciona">
					<?php 
						$search_img_ilustrativa = rwmb_meta('Gran_servico_paid_search_img_ilustrativa');
						foreach($search_img_ilustrativa as $search_img_ilustrativa):
							$search_img_ilustrativa = $search_img_ilustrativa['full_url'];				
					?>
					<figure>
						<img src="<?php echo $search_img_ilustrativa ?>" alt="<?php echo $search_img_ilustrativa ?>">
					</figure>
					<?php endforeach;?>

					<article  class="hand-formatacao-texto hand-subpage-sub-title">
						<?php echo rwmb_meta('Gran_servico_paid_search_texto_como_funciona');?>
					</article>	
				</div>
			</div>
			<div class="col-md-6">
				<div class="hand-prod-mod-content-text-area-como-funciona content-right">
					<span class="hand-formatacao-texto"><?php echo rwmb_meta('Gran_servico_paid_search_titulo_ciclo');?></span>

					<ul>
						<?php 
							$i = 1;
							$servico_paid_search_list_ciclos = rwmb_meta('servico_paid_search_list_ciclo');
							foreach($servico_paid_search_list_ciclos as $servico_paid_search_list_ciclos):
								$servico_paid_search_list_ciclo = $servico_paid_search_list_ciclos;
						?>
						<li>
							<strong><?php echo $i ?></strong>
							<h2><?php echo $servico_paid_search_list_ciclo['titulo'] ?></h2>
							<p><?php echo $servico_paid_search_list_ciclo['texto'] ?></p>
						</li>
						<?php  $i++; endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section class="hand-prod-mod-content-text-area-how-it-works">
		<div class="hand-title-prod">
			<h3 class="hand-title-destaque-center hand-tile-responsive"><?php echo rwmb_meta('Gran_servico_paid_search_titulo_How_It_Works');?></h3>
			<p><?php echo rwmb_meta('Gran_servico_paid_search_texto_How_It_Works');?></p>
		</div>

		<h2>Ciclo PPC Handgran</h2>

		<ul>
			<li class="hand-list-left-border">
				<div class="hand-conteuto">
					<figure>
						<img src="<?php echo rwmb_meta('servico_paid_search_img_Ciclo_Planejamento')['full_url'];?>" alt="<?php echo rwmb_meta('servico_paid_search_img_Ciclo_Planejamento');?>">
					</figure>
					<h3><?php echo rwmb_meta('Gran_servico_paid_search_titulo_Ciclo_Planejamento');?></h3>
					<?php 
						$servico_paid_search_lista_Ciclos = rwmb_meta('Gran_servico_paid_search_lista_Ciclo_Planejamento');
						foreach($servico_paid_search_lista_Ciclos as $servico_paid_search_lista_Ciclos):
							$servico_paid_search_lista_Ciclo = $servico_paid_search_lista_Ciclos;
					?>
					<p><?php echo $servico_paid_search_lista_Ciclo ?></p>
				<?php endforeach; ?>
				</div>
			</li>
			<li class="hand-list-right-border">
				
			</li>
			<li class="hand-list-left-border">
				<div class="hand-conteuto">
					<figure>
						<img src="<?php echo rwmb_meta('servico_paid_search_img_Ciclo_Criacao')['full_url'];?>" alt="<?php echo rwmb_meta('servico_paid_search_img_Ciclo_Criacao');?>">
					</figure>
					<h3><?php echo rwmb_meta('Gran_servico_paid_search_titulo_Ciclo_Criacao');?></h3>
					<?php 
						$servico_paid_search_lista_Ciclos = rwmb_meta('Gran_servico_paid_search_lista_Ciclo_Criacao');
						foreach($servico_paid_search_lista_Ciclos as $servico_paid_search_lista_Ciclos):
							$servico_paid_search_lista_Ciclo = $servico_paid_search_lista_Ciclos;
					?>
					<p><?php echo $servico_paid_search_lista_Ciclo ?></p>
				<?php endforeach; ?>
				</div>
			</li>
			<li class="hand-list-right-border">
				
			</li>
			<li class="hand-list-left-border">
				<div class="hand-conteuto">
					<figure>
						<img src="<?php echo rwmb_meta('servico_paid_search_img_Ciclo_Otimizacao')['full_url'];?>" alt="<?php echo rwmb_meta('servico_paid_search_img_Ciclo_Otimizacao');?>">
					</figure>
					<h3><?php echo rwmb_meta('Gran_servico_paid_search_titulo_Ciclo_Otimizacao');?></h3>
					<?php 
						$servico_paid_search_lista_Ciclos = rwmb_meta('Gran_servico_paid_search_lista_Ciclo_Otimizacao');
						foreach($servico_paid_search_lista_Ciclos as $servico_paid_search_lista_Ciclos):
							$servico_paid_search_lista_Ciclo = $servico_paid_search_lista_Ciclos;
					?>
					<p><?php echo $servico_paid_search_lista_Ciclo ?></p>
					<?php endforeach; ?>
				</div>
			</li>
			<li class="hand-list-right-border">
				
			</li>
			<li class="hand-list-left-border">
				<div class="hand-conteuto">
					<figure>
						<img src="<?php echo rwmb_meta('servico_paid_search_img_Ciclo_Mensuracao')['full_url'];?>" alt="<?php echo rwmb_meta('servico_paid_search_img_Ciclo_Mensuracao');?>">
					</figure>
					<h3><?php echo rwmb_meta('Gran_servico_paid_search_titulo_Ciclo_Mensuracao');?></h3>
					<?php 
						$servico_paid_search_lista_Ciclos = rwmb_meta('Gran_servico_paid_search_lista_Ciclo_Mensuracao');
						foreach($servico_paid_search_lista_Ciclos as $servico_paid_search_lista_Ciclos):
							$servico_paid_search_lista_Ciclo = $servico_paid_search_lista_Ciclos;
					?>
					<p><?php echo $servico_paid_search_lista_Ciclo ?></p>
					<?php endforeach; ?>
				</div>
			</li>
		</ul>
	</section>

	<section class="hand-prod-mod-content-text-Our-goals-for-your-PPC">
		<div class="row">
			
			<div class="col-sm-5">
				
				<div class="hand-title-page">
					<figure>
				 	    <img src="<?php echo get_template_directory_uri(); ?>/img/heartGran.svg" alt="heartGran">
					</figure>
					<h3><?php echo rwmb_meta('Gran_servico_paid_search_Ourgoals_titulo');?></h3>
				</div>
			</div>		
			<div class="col-sm-7">
				<div class="hand-page-content-text formatacaoLista">
					<ul>
						<?php 
						$servico_paid_search_Ourgoals_lista = rwmb_meta('Gran_servico_paid_search_Ourgoals_lista');
						foreach($servico_paid_search_Ourgoals_lista as $servico_paid_search_Ourgoals_lista):
							$servico_paid_search_Ourgoals_lista = $servico_paid_search_Ourgoals_lista;
					?>
					<li>
						<p><?php echo $servico_paid_search_Ourgoals_lista ?></p>
					</li>
					<?php endforeach; ?>
					</ul>
				</div>
			</div>

		</div>
	</section>

	<?php 
		// TEMPLATE BLOG
		include (TEMPLATEPATH . '/templates/blog.php'); 
	?>

	<div class="hand-containerLarge">
		<section class="hand-info-prod">
			<span><?php echo rwmb_meta('Gran_servico_paid_search_titulo_rodape'); ?></span>
			<h3><?php echo rwmb_meta('Gran_servico_paid_search_texto_rodape'); ?></h3>
			<a class="" href="<?php echo rwmb_meta('Gran_servico_paid_search_link_rodape'); ?>">Me Ajude a investir certo</a>
		</section>
	</div>
</div>