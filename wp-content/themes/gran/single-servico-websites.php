<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gran
 */

global $post;

//FOTO DESTACADA
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];

//RECUPERANDO NOME TAXONOMIA 
$terms = get_the_terms( $post->ID, 'categoriaservicos');
//TÍTULO POST ATUAL
$post_title = $post->post_title;

//CLASSE ATIVO
$ativo_item = "ativo-item";

//QUERY CUSTON POST TYPE/ RECUPERANDO SERVIÇOS
$servicos = new WP_Query(
					array(
						'post_type'     => 'servico',
						'ordeby'        => 'id',
						'order'         => 'asc',
						'post_per_page' => 1,
						'tax_query'     => array(
						array(
							'taxonomy' => 'categoriaservicos',
							'field'    => 'slug',
							'terms'    => $terms[0]->slug ,
							)
						)

					)
				);
get_header();

// TEMPLATE E-commerce Marketing -> hand-prod-mod-1
include (TEMPLATEPATH . '/templates/hand-prod-mod-6.php');

get_footer();
