<?php

/**
 * Template Name: Vagas
 * Description: Vagas
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gran
 */



get_header(); ?>

<div class="pg hand-vaga">
	<section class="hand-perguntas-frequentes-area">
		
		<div class="hand-titulo-page">
			<div class="hand-container">
				<h2><?php echo get_the_title() ?></h2>
				<p><?php echo $configuracao['gran_config_vagas_subtitulo'] ?></p>
			</div>
		</div>

		<article>
			<div class="row">
				
				<div class="col-sm-6">
					<figure class="template-desktop">
						<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
					</figure>
				</div>
				<div class="col-sm-6">
					<div class="hand-area-conteudo formatacaoLista">
						<?php 
							if ( have_posts() ) : while( have_posts() ) : the_post();
								
								echo the_content();

							endwhile;endif; wp_reset_query(); ?>		
						
						<figure class="template-mobile">
								<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
						</figure>
					</div>

					<div class="formulario">
						<h3><?php echo $configuracao['gran_config_vagas_titulo_formulario'] ?></h3>
						<div>
						
							<?php echo do_shortcode($configuracao['gran_config_vagas_script_formulario'] ); ?>
						</div>
					</div>
				</div>

			</div>

		</article>

	</section>



</div>
<?php get_footer();