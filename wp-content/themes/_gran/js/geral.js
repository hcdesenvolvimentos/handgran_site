
$(function(){
	

  	


	/*****************************************
		SCRIPTS CARROSSEL DESTAQUE
	*******************************************/
	$("#carrosselprojetoHome").owlCarousel({
		items : 1,
        dots: true,
        loop: true,
        lazyLoad: false,
        mouseDrag:false,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,
	    animateOut: 'fadeOut',

	});
	//BOTÕES DO CARROSSEL DESTAQUE HOME
	var carrossel_projetos = $("#carrosselprojetoHome").data('owlCarousel');
	$('.botao.carrosselProjetosEsquerda_home').click(function(){ carrossel_projetos.prev(); });
	$('.botao.carrosselProjetosDireita_home').click(function(){ carrossel_projetos.next(); });


	/*****************************************
		SCRIPTS CARROSSEL DESTAQUE
	*******************************************/
	$("#carrosselProjetosGaleria").owlCarousel({
		items : 2,
        dots: true,
        loop: false,
        lazyLoad: false,
        mouseDrag:true,
        touchDrag  : true,	
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,
	    center:true,

	});
	//BOTÕES DO CARROSSEL DESTAQUE HOME
	var carrossel_galeria = $("#carrosselProjetosGaleria").data('owlCarousel');
	$('.carrosselProjetosDireita').click(function(){ carrossel_galeria.next(); });


	$("#carrosselMenuItem").owlCarousel({
		items : 4,

        dots: true,
        loop: false,
        lazyLoad: false,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:false,
	    smartSpeed: 450,


	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            320:{
                items:1,
               
            },
            460:{
                items:2,
               
            },
            768:{
                items:3,
               
            },
           
            991:{
                items:3,
               
            },
            1024:{
                items:4
            },
            1440:{
                items:4
            },
            			            
        }		    		   		    
	    
	});

	

	 $('.hand-prod-mod-5 .hand-naming-categoria .hand-naming-area-categoria .hand-naming-area-categoria-list ul li').click(function(e) {

      let dataID = $(this).attr("data-id");

      $('.hand-prod-mod-5 .hand-naming-categoria .hand-naming-area-categoria .hand-naming-area-categoria-list.area-logo').hide();

      $( ".hand-prod-mod-5 .hand-naming-categoria .hand-naming-area-categoria .hand-naming-area-categoria-list.area-logo#" + dataID ).show();

      $('.hand-prod-mod-5 .hand-naming-categoria .hand-naming-area-categoria .hand-naming-area-categoria-list ul li').removeClass( "ativo" );

      $(this).addClass( "ativo" );

    });





	 

	$( ".hand-contato-float" ).click(function() {
		 $(".hand-contato-float").slideUp();
		  $("header .hand-menu-home> ul.menu-nav li>a").show();
		  $("div.pg.hand-home>section article").removeClass("ativarSombra")
		  $("body").removeClass("background-menu");
		  setTimeout(function(){
		  $(".areaDestaqueSombra").removeClass("ativarSombra_destaque");
		}, 1000);
	}); 

  

  	$( "div.pg.hand-home>section.hand-oqueFazemos-home ul li" )
	  .mouseover(function(e) {
	  	$("div.pg.hand-home>section.hand-oqueFazemos-home ul li").removeClass("ativo");
	   	$(this).addClass("ativo");
	  })
	  .mouseout(function(e) {
	   $(this).removeClass("ativo");
	   $("div.pg.hand-home>section.hand-oqueFazemos-home ul li:nth-child(2n)").addClass("ativo");
  	});

	 $( "header .hand-menu-home> ul.menu-nav li" ).click(function(e) {
			event.preventDefault();
		$(".hand-mega-menu").addClass("slideDown");
		
		$("body").addClass("background-menu");
		 setTimeout(function(){
			$("header .hand-button-menu-mobile button").addClass("slideDown-button");
		}, 800);

		setTimeout(function(){
			$(".hand-mega-menu .row").addClass("opacity-ativo");
		}, 200);
	}); 


  	$( "header .hand-button-menu-mobile button" ).click(function() {
		$(".hand-mega-menu .row").toggleClass("opacity-ativo");
		$("body").toggleClass("background-menu");
		 setTimeout(function(){
			$("header .hand-button-menu-mobile button").toggleClass("slideDown-button");
			$(".hand-mega-menu").toggleClass("slideDown");
		}, 200);
	});


});




