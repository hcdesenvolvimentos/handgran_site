<div class="pg hand-prod-mod-2">
			
	<section class="hand-destaque-prod">
		<img src="<?php echo get_template_directory_uri(); ?>/img/grid-fundo.svg">
		<div class="hand-containerLarge">
			<article>
				<span class="hand-subTitle-topo"><?php echo $terms[0]->name; ?></span>
				<h1 class="hand-title-destaque"><?php echo get_the_title(); ?></h1>
				<p class="hand-paragrafo-deatque"><?php echo rwmb_meta('servico_titulo_im_Inbound_Marketing'); ?></p>
			</article>

			<nav>
				<ul id="carrosselMenuItem" class="owl-Carousel">
					<?php while($servicos->have_posts()) : $servicos->the_post(); ?>
					<li class="item  <?php if($post_title == get_the_title()){ echo $ativo_item;}; ?> ">
						<a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
					</li>
					<?php endwhile;wp_reset_query(); ?>
				</ul>
			</nav>
		</div>
	</section>

	<section class="hand-inbound-marketing-prod">
		<div class="hand-containerLarge">
			<div class="row">
				<div class="col-sm-6">
					<figure>
						<?php if ($foto): ?>
						<img src="<?php echo $foto ?>" alt="<?php  echo get_the_title(); ?> ">
						<?php  endif; ?>
						<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>	
					</figure>
				</div>
				<div class="col-sm-6">
					<article class="hand-formatacao-texto template-desktop">
						<?php echo rwmb_meta('servico_texto_im_Inbound_Marketing'); ?>		
					</article>
				</div>
			</div>
		</div>

		<article class="hand-formatacao-texto template-mobile">
			<?php echo rwmb_meta('servico_texto_im_Inbound_Marketing'); ?>	
		</article>

			</section>

			<div class="hand-inbound-marketing-prod-info">
				<ul>
					<li>
						<?php 
							$icone = rwmb_meta('servico_icone_box_esquerdo_Inbound_Marketing');
							if ($icone['full_url']):
						?>
						<figure>
							<img src="<?php echo $icone['full_url']; ?>" alt="<?php echo $icone['full_url']; ?>">							
						</figure>
						<?php endif; ?>
						
						<p><?php echo rwmb_meta('servico_texto_box_esquerdo_Inbound_Marketing'); ?></p> 
					</li>
					<li>
						<?php 
							$icone = rwmb_meta('servico_icone_box_direito_Inbound_Marketing');
							if ($icone['full_url']):
						?>
						<figure>
							<img src="<?php echo $icone['full_url']; ?>" alt="<?php echo $icone['full_url']; ?>">							
						</figure>
						<?php endif; ?>
						<p><?php echo rwmb_meta('servico_texto_box_direito_Inbound_Marketing'); ?></p>
					</li>
				</ul>
			</div>

			<section class="hand-how-it-works-prod">
				<div class="hand-title-prod">
					<span><?php echo rwmb_meta('servico_titulo_plano_tatico_Inbound_Marketing'); ?></span>
					<h3 class="hand-title-destaque-center"><?php echo rwmb_meta('servico_Subtitulo_plano_tatico_Inbound_Marketing'); ?></h3>
				</div>

				<div class="hand-containerLarge">
					<div class="hand-contant-prod">
						<div class="row">
							<div class="col-sm-6">
								<ul>
									<?php 
										$listItens =  rwmb_meta('servico_lis_plano_tatico_Inbound_Marketing');  
										foreach ($listItens as $listItens):
									?>
									<li>
										<h3><?php echo $listItens['titulo'] ?></h3>
										<p><?php echo $listItens['basic'] ?></p>
									</li>
									<?php endforeach; ?>
								</ul>
							</div>
							<div class="col-sm-6">
								<figure>
									<?php 
										$imgIlustrativa =  rwmb_meta('servico_ilustrativo_plano_tatico_Inbound_Marketing'); 
										if ($imgIlustrativa['full_url']):
									?>
									<img src="<?php echo $imgIlustrativa['full_url'] ?>" alt="<?php echo $imgIlustrativa['full_url'] ?>">
									<figcaption class="hidden"><?php echo $imgIlustrativa['full_url'] ?></figcaption>
									<?php endif; ?>	
								</figure>
							</div>
						</div>
					</div>
				</div>

			</section>

			<section class="hand-como-funciona-inbound-prod">
			
				<div class="row">
					<div class="col-md-7">
						<article class="hand-formatacao-texto">
							<h2><?php echo rwmb_meta('servico_titulo_como_funciona_Inbound_Marketing'); ?></h2>
							<p><?php echo rwmb_meta('servico_texto_como_funciona_Inbound_Marketing'); ?></p>
							<?php 
								$fotoIlustrativaComoFunciona = rwmb_meta('servico_ilustrativo_como_funciona_Inbound_Marketing');
								if ($fotoIlustrativaComoFunciona['full_url']): 
							?>
							<figure class="template-mobile">
								<img src="<?php echo $fotoIlustrativaComoFunciona['full_url'] ?> "class="img-ilustrativa" alt="<?php echo $fotoIlustrativaComoFunciona['full_url'] ?>">
								<figcaption class="hidden"><?php echo $fotoIlustrativaComoFunciona['full_url'] ?></figcaption>	
							</figure>
							<?php endif; ?>
							<ul>
								<?php 

									$listComofunciona = rwmb_meta('servico_list_como_funciona_Inbound_Marketing');
									$iconeListComofunciona = rwmb_meta('servico_list_icone_como_funciona_Inbound_Marketing');
									$cont = 0;
									foreach ($listComofunciona as $listComofunciona):
								?>
								<li>
									<figure>
										<img src="<?php  echo $iconeListComofunciona[$cont]['full_url'] ?>">
										<figcaption class="hidden"><?php  echo $iconeListComofunciona[$cont]['full_url'] ?></figcaption>	
									</figure>										
									<h3><?php echo $listComofunciona['titulo'] ?></h3>
									<p><?php echo $listComofunciona['texto'] ?></p>
								</li>
								<?php $cont++;endforeach; ?>
								
							</ul>
						</article>
					</div>
					<div class="col-md-5">
						<?php 
							if ($fotoIlustrativaComoFunciona['full_url']): 
						?>
						<figure class="template-desktop">
							<img src="<?php echo $fotoIlustrativaComoFunciona['full_url'] ?> "class="img-ilustrativa" alt="<?php echo $fotoIlustrativaComoFunciona['full_url'] ?>">
							<figcaption class="hidden"><?php echo $fotoIlustrativaComoFunciona['full_url'] ?></figcaption>	
						</figure>
						<?php endif; ?>
					</div>
				</div>
				
			</section>

			<section class="hand-360-prod">
				<article>
					<div class="row">
						<div class="col-md-7 text-right">
							<div class="hand-content-text-prod content-left">
								<div class="formatacaoLista">
									<h2><?php echo $titulo360 = rwmb_meta('servico_titulo_360_Inbound_Marketing'); ?></h2>
									<p><?php echo $texto360 = rwmb_meta('servico_texto_360_Inbound_Marketing'); ?></p>
									<ul>
										<?php  
											$list360 = rwmb_meta('servico_list_360_Inbound_Marketing'); 
											foreach ($list360 as $list360):
										?>
										<li>
											<h3><?php echo $list360 ?></h3>
										</li>
										<?php endforeach; ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<?php 
								$iconeCanaisEstrategia = rwmb_meta('servico_icone_canais_estrategia_Inbound_Marketing'); 
								if ($iconeCanaisEstrategia['full_url']):
							?>
							<div class="hand-content-text-prod content-right">
								<img src="<?php echo $iconeCanaisEstrategia['full_url'] ?>" alt="<?php echo $iconeCanaisEstrategia['full_url'] ?>">
								<?php echo rwmb_meta('servico_texto_canais_estrategia_Inbound_Marketing');  ?>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</article>
			</section>

			<section class="hand-inbound-negocio-prod">
				<article class="hand-containerLarge">
					<div class="hand-title-prod">
						<h3><?php echo rwmb_meta('servico_titulo_qualquer_negocio_Inbound_Marketing');  ?></h3>
					</div>

					<div class="row">
						<div class="col-sm-6">
							<div class="hand-content-text-prod">
								<?php echo rwmb_meta('servico_texto_qualquer_negocio_esquerdo_Inbound_Marketing');  ?>
							</div>
						</div>			
						<div class="col-sm-6">
							<div class="hand-content-text-prod">
								<?php echo rwmb_meta('servico_texto_qualquer_negocio_direita_Inbound_Marketing');  ?>
							</div>
						</div>			
					</div>
				</article>
			</section>

			<div class="hand-containerLarge">
				<section class="hand-info-prod">
					<span><?php echo rwmb_meta('servico_frase_titulo_rodape_Inbound_Marketing');  ?></span>
					<?php echo rwmb_meta('servico_frase_texto_rodape_Inbound_Marketing');  ?>
				</section>
			</div>

		</div>