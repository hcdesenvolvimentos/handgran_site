<div class="pg hand-prod-mod-4">
			
			<section class="hand-destaque-prod">
				<img src="img/grid-fundo.svg">
				<div class="hand-containerLarge">
					<article>
						<span class="hand-subTitle-topo">Strategy</span>
						<h1 class="hand-title-destaque">Social Media Marketing</h1>
						<p class="hand-paragrafo-deatque"><?php echo rwmb_meta('Gran_servico_subtitulo_social_Media_Marketing'); ?></p>
					</article>

					<nav>
						<ul id="carrosselMenuItem" class="owl-Carousel">
							<?php while($servicos->have_posts()) : $servicos->the_post(); ?>
							<li class="item  <?php if($post_title == get_the_title()){ echo $ativo_item;}; ?> ">
								<a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
							</li>
							<?php endwhile;wp_reset_query(); ?>
						</ul>
					</nav>
				</div>
			</section>

			<section class="hand-social-media-description">
				<div class="row">
					<div class="col-sm-7 text-right">
						<article>
							<?php echo rwmb_meta('Gran_servico_texto_social_Media_Marketing'); ?>
						</article>
					</div>
					<div class="col-sm-5">
						<?php 
							$servico_foto_social_Media_Marketing = rwmb_meta('Gran_servico_foto_social_Media_Marketing'); 
							if($servico_foto_social_Media_Marketing):
						?>
						<figure>
							<img src="<?php echo $servico_foto_social_Media_Marketing['full_url']; ?>">
						</figure>
						<?php endif;?>
					</div>
				</div>
			</section>

			<section class="hand-social-media-description-smm">
				<div class="hand-containerLarge">
					<div class="row">
						<div class="col-md-6">
							<?php 
								$servico_foto_social_Media_Marketing_SMM = rwmb_meta('Gran_servico_foto_social_Media_Marketing_SMM'); 
								if($servico_foto_social_Media_Marketing_SMM):
							?>
							<figure>
								<img src="<?php echo $servico_foto_social_Media_Marketing_SMM['full_url']; ?>" alt="<?php echo $servico_foto_social_Media_Marketing_SMM ?>">
							</figure>
							<?php endif;?>
						</div>	
						<div class="col-md-6">
							<article>
								<?php echo  rwmb_meta('Gran_servico_foto_social_Media_Marketing_SMM_texto'); ?>
							</article>
							<?php 
								$servico_foto_social_Media_Marketing_SMM = rwmb_meta('Gran_servico_foto_social_Media_Marketing_SMM'); 
								if($servico_foto_social_Media_Marketing_SMM):
							?>
							<figure class="template-mobile">
								<img src="<?php echo $servico_foto_social_Media_Marketing_SMM['full_url']; ?>" alt="<?php echo $servico_foto_social_Media_Marketing_SMM ?>">
							</figure>
							<?php endif;?>
						</div>	
					</div>
				</div>
			</section>

			<div class="hand-containerFull">
				<section class="hand-social-media-description-ads">
					<div class="row">
						<div class="col-sm-5">
							<div class="hand-media-titulo">
								<figure>
									<img src="<?php echo get_template_directory_uri(); ?>/img/thumbs-up.svg">
								</figure>
								<h3><?php echo rwmb_meta('Gran_servico_foto_social_Media_Marketing_box_titulo');  ?></h3>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="hand-social-media-description-ads-conteudo">
								<p><?php echo rwmb_meta('Gran_servico_foto_social_Media_Marketing_box_texto');  ?></p>
							</div>
						</div>
					</div>
				</section>
			</div>

			<section class="hand-social-media-description-instagram-ads">
				<div class="hand-containerLarge">
					<div class="row">
						<div class="col-sm-8">
							<article>
								<h6><?php echo rwmb_meta('Gran_servico_foto_social_Media_Marketing_Instagram_titulo');  ?></h6>
								<p><?php echo rwmb_meta('Gran_servico_foto_social_Media_Marketing_Instagram_texto');  ?></p>
							</article>
						</div>	
						<div class="col-sm-4">
							<?php 
								$servico_foto_social_Media_Marketing_Instagram_foto = rwmb_meta('Gran_servico_foto_social_Media_Marketing_Instagram_foto'); 
								if($servico_foto_social_Media_Marketing_Instagram_foto):
							?>
							<figure>
								<img src="<?php echo $servico_foto_social_Media_Marketing_Instagram_foto['full_url']; ?>" alt="<?php echo $servico_foto_social_Media_Marketing_Instagram_foto ?>">
							</figure>
							<?php endif;?>
						</div>	
					</div>
				</div>
			</section>

			<?php 
				// TEMPLATE BLOG
				include (TEMPLATEPATH . '/templates/blog.php'); 
			?>

			<div class="hand-containerLarge">
				<section class="hand-info-prod">
					<?php echo rwmb_meta('Gran_servico_foto_social_Media_Marketing_frase_odape');  ?>
					<a class="" href="<?php echo rwmb_meta('Gran_servico_foto_social_Media_Marketing_frase_link');  ?>"><?php echo rwmb_meta('Gran_servico_foto_social_Media_Marketing_frase_link_texto');  ?></a>
				</section>
			</div>

		</div>