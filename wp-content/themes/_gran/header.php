<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gran
 */
global $configuracao;

// VERIFICAÇÃO MENU
if (get_page_link() != get_home_url()."/" && get_page_link() != get_home_url()."/en/" ) {
	$formatacao_header = "hand-header-internas";
}


?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
<header class="<?php if($formatacao_header != ""){echo $formatacao_header;}; ?>">
	
	<div class="header-content-area">
		<div class="row hand-containerFull">
			<div class="col-sm-1">
				<?php if (trim($configuracao['gran_header_logo']['url'])): ?>
				<figure>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img src="<?php echo $configuracao['gran_header_logo']['url']; ?>" alt="<?php echo bloginfo(); ?>">
						<figcaption class="hidden"><?php echo bloginfo(); ?></figcaption>	
					</a>
				</figure>
				<?php endif; ?>
			</div>	
			<div class="col-sm-11">
				<div class="hand-button-menu-mobile">
					<button></button>
				</div>
				<div class="hand-menu-home">
					<ul class="menu-nav">
					<?php 
						
						wp_nav_menu( array('menu' => "Header Principal",));
						
					?>
					</ul>
				</div>
			</div>	
		</div>
	</div>

	<div class="hand-mega-menu">
		<div class="row">
			<div class="col-sm-7">	
				<ul class="menu-navegation">
					<li class="home">
						<img src="<?php echo get_template_directory_uri(); ?>/img/flexa.svg">
						<h2>
							<a href="<?php echo esc_url( home_url( '/servicos/e-commerce-marketing/' ) ); ?>">Strategy</a>
						</h2>
						<p><a href="<?php echo esc_url( home_url( '/servicos/e-commerce-marketing/' ) ); ?>">E-commerce Marketing</a></p>
						<p><a href="<?php echo esc_url( home_url( '/servicos/inbound-marketing/' ) ); ?>">Inbound Marketing</a></p>
						<p><a href="<?php echo esc_url( home_url( '/servicos/paid-search/' ) ); ?>">Mídias Pagas</a></p>
						<p><a href="<?php echo esc_url( home_url( '/servicos/social-media-marketing/' ) ); ?>">Social Media Marketing</a></p>
					</li>
					<li>
						<img src="<?php echo get_template_directory_uri(); ?>/img/flexa.svg">
						<h2>
							<a href="<?php echo esc_url( home_url( '/servicos/design-naming/' ) ); ?>">Design</a>
						</h2>
						<p><a href="<?php echo esc_url( home_url( '/servicos/design-naming/' ) ); ?>">Naming</a></p>
						<p><a href="">Branding Concept</a></p>
						<p><a href="">Design de Interface</a></p>
					</li>
					<li>
						<img src="<?php echo get_template_directory_uri(); ?>/img/flexa.svg">
						<h2>
							<a href="">Dev</a>
						</h2>
						<p><a href="">Website</a></p>
						<p><a href="">Fast Sites</a></p>
						<p><a href="">Implantação e-commerce</a></p>
					</li>
				</ul>
			</div>
			<div class="col-sm-5">
				<div class="hand-contato-float-menu">
					<ul>
						<li class="mobile-item-menu">
							<a href="<?php echo esc_url( home_url( '/servicos/e-commerce-marketing/' ) ); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cafe.svg">Strategy
							</a>
						</li>
						<li class="mobile-item-menu">
							<a href="<?php echo esc_url( home_url( '/servicos/design-naming/' ) ); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cafe.svg">Design
							</a>
						</li>
						<li class="mobile-item-menu">
							<a href="">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cafe.svg">Dev
							</a>
						</li>
						<li>
							<a href="<?php echo esc_url( home_url( '/land-page/' ) ); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cafe.svg">Estudo de caso
							</a>
						</li>
						<li>
							<a href="">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cafe.svg">O que fazemos		
							</a>
						</li>
						<li>
							<a href="<?php echo esc_url( home_url( '/gran/' ) ); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cafe.svg">A Agência
							</a>
						</li>
						<li>
							<a href="">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cafe.svg">Blog
							</a>
						</li>
						<li>
							<a href="">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cafe.svg">Contato
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>
