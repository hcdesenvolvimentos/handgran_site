<?php

/**
 * Template Name: Inicial
 * Description: Página Inicial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gran
 */
	$parceiros = new WP_Query( array( 'post_type' => 'parceiro', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
	$projetos  = new WP_Query( array( 'post_type' => 'projetos', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
get_header(); ?>

<!-- DIV PÁGINA INICIAL -->
<div class="pg hand-home">

	<!-- SESSÃO DESTAQUE -->
	<section class="hand-destaque-home">
		<div class="hand-container">
			<article>
				<?php  if (trim($configuracao['gran_config_inicial_destaque_titulo'])):?>
				<h3 class="hand-title-destaque"><?php echo $configuracao['gran_config_inicial_destaque_titulo'] ?></h3>
				<p class="hand-paragrafo-deatque"><?php echo $configuracao['gran_config_inicial_destaque_texto'] ?></p>
				<a href="<?php echo $configuracao['gran_config_inicial_destaque_link'] ?>" class="hand-link-page">Quero começar agora!</a>
				<?php endif; ?>
			</article>
		</div>
	</section>

	<section class="hand-oqueFazemos-home">
		<h3 class="hidden"><?php echo $configuracao['gran_config_inicial_oqueFazemos_titulo'] ?></h3>

		<div class="hand-container">
			<div class="row">
				<div class="col-sm-6">
					<div class="hand-title-section-home">
						<span><?php echo $configuracao['gran_config_inicial_oqueFazemos_titulo'] ?></span>
					</div>
				</div>
				<div class="col-sm-6">
					<article class="hand-formatacao-texto">
						<p><?php echo $configuracao['gran_config_inicial_oqueFazemos_texto'] ?></p>
					</article>
				</div>
			</div>
		</div>

		<ul>
			<?php if(trim($configuracao['gran_config_inicial_servicos_titulo'] )): ?>
			<li>
				<a href="<?php echo $configuracao['gran_config_inicial_servicos_link1'] ?>">
					<figure>
						<img src="<?php echo $configuracao['gran_config_inicial_servicos_icone']['url'] ?>" alt="<?php echo $configuracao['gran_config_inicial_servicos_titulo'] ?>">
						<figcaption class="hidden"><?php echo $configuracao['gran_config_inicial_servicos_titulo'] ?></figcaption>	
					</figure>
					<h2><?php echo $configuracao['gran_config_inicial_servicos_titulo'] ?></h2>
					<p><?php echo $configuracao['gran_config_inicial_servicos_texto'] ?></p>
					<span>Saiba mais</span>
				</a>
			</li>
			<?php endif; if(trim($configuracao['gran_config_inicial_servicos_titulo_2'] )): ?>
			<li class="ativo">
				<a href="<?php echo $configuracao['gran_config_inicial_servicos_link2'] ?>">
					<figure>
						<img src="<?php echo $configuracao['gran_config_inicial_servicos_icone_2']['url'] ?>" alt="<?php echo $configuracao['gran_config_inicial_servicos_titulo_2'] ?>">
						<figcaption class="hidden"><?php echo $configuracao['gran_config_inicial_servicos_titulo_2'] ?></figcaption>	
					</figure>
					<h2><?php echo $configuracao['gran_config_inicial_servicos_titulo_2'] ?></h2>
					<p><?php echo $configuracao['gran_config_inicial_servicos_texto_2'] ?></p>
					<span>Saiba mais</span>
				</a>
			</li>
			<?php endif; if(trim($configuracao['gran_config_inicial_servicos_titulo_3'] )): ?>
			<li>
				<a href="<?php echo $configuracao['gran_config_inicial_servicos_link3'] ?>">
					<figure>
						<img src="<?php echo $configuracao['gran_config_inicial_servicos_icone_3']['url'] ?>" alt="<?php echo $configuracao['gran_config_inicial_servicos_titulo_3'] ?>">
						<figcaption class="hidden"><?php echo $configuracao['gran_config_inicial_servicos_titulo_3'] ?></figcaption>	
					</figure>
					<h2><?php echo $configuracao['gran_config_inicial_servicos_titulo_3'] ?></h2>
					<p><?php echo $configuracao['gran_config_inicial_servicos_texto_3'] ?></p>
					<span>Saiba mais</span>
				</a>
			</li>
			<?php endif;?>
		</ul>
		
	</section>

	<section class="hand-info-home">
		<h3 class="hidden"><?php echo $configuracao['gran_config_inicial_info_titulo'] ?></h3>
		<div class="background-fundo  hand-containerFull">
		<div class="hand-container">
			<div class="row">
				<div class="col-sm-6">
					<span><?php echo $configuracao['gran_config_inicial_info_titulo'] ?></span>
				</div>
				<div class="col-sm-6">
					<article class="hand-formatacao-texto">
						<p><?php echo $configuracao['gran_config_inicial_info_texto'] ?></p>
					</article>
				</div>
			</div>
			</div>
		</div>
	</section>

	<?php if ($parceiros): ?>
	<section class="hand-parceirosTecnologia-home">
		<h3 class="hand-title-destaque-center"><?php echo $configuracao['gran_config_inicial_parceiro_titulo'] ?></h3>
		<div class="hand-container">
			<ul>
			<?php while ( $parceiros->have_posts() ) : $parceiros->the_post();?>
				<li>
					<figure>
						<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
						<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>	
					</figure>
				</li>
			<?php endwhile; ?>
			</ul>
		</div>
	</section>
	<?php endif; ?>

	<?php if ($projetos): ?>
	<section class="hand-ultimosProjetos-home">
		<h3 class="hidden">ùltimos Projetos</h3>
		
		<div id="carrosselprojetoHome">

			<?php while ( $projetos->have_posts() ) : $projetos->the_post();?>
			<div class="item">
				<div class="hand-containerFull">
					<div class="hand-galeria-projetos-home">
						<h6 class="hidden">Carrossel Destaque</h6>
						<div class="carrosselDestaque">	
							<?php 
								$i = 0;
								$projeto_lista_servicos_galeria = rwmb_meta('projeto_lista_servicos_galeria');
								foreach ($projeto_lista_servicos_galeria  as $projeto_lista_servicos_galeria):
									if($i<4):
							?>
							<div class="item-lista">
								<a href="<?php echo get_permalink(); ?>">
									<figure>
										<img src="<?php echo $projeto_lista_servicos_galeria['full_url']?>" alt="<?php echo get_the_title(); ?>">
										<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>	
									</figure>
								</a>
							</div>
							<?php endif;$i++;endforeach; ?>
						</div>
						
					</div>

					<div class="row">
						<div class="col-sm-6">
							<article class="hand-formatacao-texto">
								<span>Últimos projetos</span>
								<div class="hand-title-section-home">
									<span><?php echo get_the_title(); ?></span>
								</div>
								<p><?php echo rwmb_meta('projeto_descricao_home'); ?></p>
								<a href="<?php echo get_permalink(); ?>" class="hand-link-page">Ver case</a>
							</article>

							
						</div>
						<div class="col-sm-6">
							<div class="hand-backgroung-projetos-home">
								<figure>
									<img src="<?php echo rwmb_meta('projeto_background')['full_url'] ?>" alt="<?php echo get_the_title(); ?>">
									<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>	
								</figure>
							</div>
						</div>
					</div>

				</div>
			</div>
			<?php endwhile; ?>

		</div>

		<div class="botao-carrossel-destaque">
			<button class="botao carrosselProjetosEsquerda_home"> </button>
	        <button class="botao carrosselProjetosDireita_home"> </button>
    	</div>
	</section>
	<?php endif; ?>

</div>

<?php get_footer();