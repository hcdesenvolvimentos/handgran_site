<?php

/**
 * Template Name: Institucional 
 * Description: Página Institucional
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package gran
 */
	$parceiros = new WP_Query( array( 'post_type' => 'parceiro', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
get_header(); ?>

<!-- DIV PÁGINA QUEM SOMOS -->
    <div class="pg pg-gran">
		
			<div class="hand-titulo-page">
				<div class="hand-container">
					<span><?php echo $configuracao['gran_config_quemSomos_destaque_subTitulo'] ?></span>
					<h2><?php echo $configuracao['gran_config_quemSomos_destaque_titulo'] ?></h2>
					<p> <?php echo $configuracao['gran_config_quemSomos_destaque_texto'] ?> </p>
				</div>
			</div>

			<section class="hand-conteudo-gran-area">
				
					<div class="row">
						<div class="col-sm-6 text-right">
							<div class="hand-conteudo-gran formatacaoLista">
								<h2>  <?php echo $configuracao['gran_config_quemSomos_fundamentos_titulo'] ?></h2>
								<p> <?php echo $configuracao['gran_config_quemSomos_fundamentos_texto'] ?></p>
								<h3> <?php echo $configuracao['gran_config_quemSomos_fundamentos_subTitulo'] ?></h3>
								<ul>
									<li>Be Real</li>
									<li>Quality Over Quantity</li>
									<li>Be Better</li>
									<li>Never Stop Learning</li>
									<li>Be Simple</li>
								</ul>
							</div>
						</div>
						<div class="col-sm-6">
							<figure>
								<img src="<?php echo $configuracao['gran_config_quemSomos_fundamentos_imagem']['url']; ?>" alt="<?php echo bloginfo(); ?>">
							</figure>
						</div>
					</div>
				
			</section>

			<div class="hand-containerFull">
				<section class="hand-info-description-gran">
					<div class="row">
						<div class="col-sm-6">
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/img/heartGran.svg">
							</figure>

							<div class="hand-info-titulo-gran">
								<h3> <?php echo $configuracao['gran_config_quemSomos_comoTrabalhamos_titulo'] ?></h3>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="hand-info-description-gran-conteudo">
								<p> <?php echo $configuracao['gran_config_quemSomos_comoTrabalhamos_texto'] ?></p>
							</div>
						</div>
					</div>
				</section>
			</div>

			<section class="hand-que-fazemos-gran">
				<h3><?php echo $configuracao['gran_config_quemSomos_QueFazemos_titulo'] ?></h3>
				<p> <?php echo $configuracao['gran_config_quemSomos_QueFazemos_subTitulo'] ?></p>
				
			
				<div class="hand-que-fazemos-escription-gran">
					<div class="hand-container">
						<div class="row">
							<div class="col-sm-6">
								<div class="hand-que-fazemos-escription-gran-text text-top">
									<img class="icone" src="img/heartGran.svg" alt="">
									<h3> <?php echo $configuracao['gran_config_quemSomos_QueFazemos_tituloTextoEsquerda'] ?></h3>
									<p> <?php echo $configuracao['gran_config_quemSomos_QueFazemos_textoEsquerda'] ?></p>
								</div>
							</div>
							<div class="col-sm-6 ">
								<div class="hand-que-fazemos-escription-gran-text background-right">
									<h3> <?php echo $configuracao['gran_config_quemSomos_QueFazemos_tituloTextoDireita'] ?></h3>
									<p><?php echo $configuracao['gran_config_quemSomos_QueFazemos_TextoDireita'] ?></p>
								</div>
							</div>
						</div>
					</div>

					<div class="row row-bottom">
						<div class="col-sm-6">
							<div class="hand-que-fazemos-escription-gran-text img ">
								<figure>
									<img src="<?php echo $configuracao['gran_config_quemSomos_QueFazemos_Descricao_imagem']['url']; ?>" alt="<?php echo bloginfo(); ?>">
								</figure>
							</div>
						</div>
						<div class="col-sm-6 corrcaoPadding">
							<div class="hand-que-fazemos-escription-gran-text bottom-text">
								<h4> <?php echo $configuracao['gran_config_quemSomos_QueFazemos_Descricao_titulo'] ?></h4>
								<span><?php echo $configuracao['gran_config_quemSomos_QueFazemos_Descricao_subTitulo'] ?></span>
								<p> <?php echo $configuracao['gran_config_quemSomos_QueFazemos_Descricao_texto'] ?></p>
							</div>
						</div>
					</div>
					
				</div>

			</section>

			<section class="hand-que-fazemos-gran-servicos">
				<div class="hand-container formatacaoLista">
					<div class="row">
						<div class="col-sm-4">
							<div>
								<figure>
									<img src="img/strategyServ.svg">
									<figcaption class="hidden">Logo</figcaption>	
								</figure>
								<h2>Strategy</h2>
								<p>Desenvolvemos campanhas de mídia digital integrada usando uma combinação de Search, display, mídia social, email, e todas as outras ferramentas disponíveis.</p>
								<ul>
									<li>E-commerce Marketing</li>
									<li>Inbound Marketing</li>
									<li>Mídias Pagas</li>
									<li>Social Media Marketing</li>
								</ul>
							</div>
						</div>
						<div class="col-sm-4">
							<div>
								<figure>
									<img src="img/featherServ.svg">
									<figcaption class="hidden">Logo</figcaption>	
								</figure>
								<h2>DESIGN</h2>
								<p>Encontramos a verdade sobre como nossos clientes enriquecem a vida de seus clientes. Obtemos uma visão do que os clientes realmente desejam e de como nossos clientes preenchem esses desejos de maneira exclusiva.</p>
								<ul>
									<li>Identidade Visual</li>
									<li>Revitalização de marca</li>
									<li>Design de Interface</li>
									<li>Wireframing e Prototipagem</li>
								</ul>
							</div>
						</div>
						<div class="col-sm-4">
							<div>
								<figure>
									<img src="img/codeServ.svg">
									<figcaption class="hidden">Logo</figcaption>	
								</figure>
								<h2>DEVELOPMENT</h2>
								<p>Transformamos o seu site em uma máquina geradora de leads. Desenvolvemos aplicações interativas e dinâmicas seguindo as regras de indexação de buscadores.</p>
								<ul>
									<li>Implantação E-commerce</li>
									<li>Hotsites</li>
									<li>Web Sites</li>
									<li>Fast Sites</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>

			<div class="hand-containerLarge">
				<section class="hand-info-prod">
					<div class="row">
						<div class="col-sm-6">
							<h3>E, então?!</h3>
						</div>			
						<div class="col-sm-6 text-right">
							<a class="left" href="">Trabalhar na Gran</a>
							<a class="" href="">Contratar a Gran</a>
						</div>			
					</div>
				</section>
			</div>
	
		</div>	

<?php get_footer();