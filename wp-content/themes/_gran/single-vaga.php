<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gran
 */

global $post;



get_header();

	include (TEMPLATEPATH . '/templates/vagas/vaga.php');

get_footer(); 
