<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package gran
 */

global $post;

get_header();
?>
<div class="pg hand-oqueFazemos">	

	<!-- SESSÃO DESTAQUE O QUE FAZEMOS -->
	<section class="hand-destaque-oqueFazemos " style="background: url(<?php echo rwmb_meta('projeto_background')['full_url'] ?>);background-size: cover!important; ">
		<div class="hand-container">
			
			<article class="hand-formatacao-texto">
				<figure>	
					<img src="<?php echo rwmb_meta('projeto_logo')['full_url'] ?>" alt="<?php echo rwmb_meta('projeto_logo')['full_url'] ?>" class="img-oqueFazemos">
					<figcaption class="hidden"> <?php echo rwmb_meta('projeto_logo')['full_url'] ?></figcaption>
				</figure>
				<p class="hand-descricao-oqueFazemos "><?php echo rwmb_meta('projeto_breve_descricao')?></p>
				<a href="" class="hand-link-page-down"> 
			    <img src="<?php echo get_template_directory_uri(); ?>/img/arrow-right.png" alt="seta">	
			    </a>
			</article>
		</div>
	</section>

	<!-- SESSÃO SERVIÇOS OFERECIDOS HAND-->
	<section class="hand-oqueFazemos-servicos">
		<h3 class="hidden"><?php echo rwmb_meta('projeto_titulo_Que_fazemos')?></h3>

		<div class="hand-container">
			<div class="row">
				<div class="col-sm-6">
					<div class="hand-title-section">
						<h2><?php echo rwmb_meta('projeto_titulo_Que_fazemos')?></h2>
					</div>
				</div>
				<div class="col-sm-6">
					<article class="hand-descricao-oqueFazemos hand-formatacao-texto">
						<p><?php echo rwmb_meta('projeto_texto_fazemos')?></p>
					</article>
					<div class="hand-title-servicos">
						<h3><?php echo rwmb_meta('projeto_titulo_servicos_realizados')?></h3>
					</div>
					<div class="hand-lista-servicos-oferecidos formatacaoLista">
						<ul>	
							<?php 

								$listServicos = rwmb_meta('projeto_lista_servicos_realizados');
								foreach ($listServicos  as $listServico):
							?>
							<li class="hand-servico"><?php echo $listServico  ?></li>
							<?php endforeach; ?>
							
						</ul>
					</div>
				</div>
				
			</div>
		</div>
	</section>

	<section class="hand-oqueFazemos-carrossel-fotos">
		<div id="carrosselProjetosGaleria">
			<?php 
				$projeto_lista_servicos_galeria = rwmb_meta('projeto_lista_servicos_galeria');
				foreach ($projeto_lista_servicos_galeria  as $projeto_lista_servicos_galeria):
			?>
			<div class="item">
				<figure>
					<img src="<?php echo $projeto_lista_servicos_galeria['full_url']?>" alt="<?php echo $projeto_lista_servicos_galeria['full_url']?>">
				</figure>
			</div>
			<?php endforeach; ?>
		</div>
		<div class="botao-carrossel-projetos">
	        <button class="botao carrosselProjetosDireita">
	        	<img src="<?php echo get_template_directory_uri(); ?>/img/arrow-right.png" alt="arrow-right">
	        </button>
		</div>
	</section>

	<!-- SESSÃO SERVIÇO SOCIAL MEDIA -->
	<section class="hand-oqueFazemos-servSocialMedia">
		<div class="hand-container">
			<div class="row">
				<div class="col-sm-6">
					<div class="hand-title-servico">
						<h2><?php echo rwmb_meta('projeto_Titulo_Social_Media_Marketing')?></h2>
					</div>
				</div>
				<div class="col-sm-6">
					<article class="hand-descricao-servico hand-formatacao-texto">
						<p><?php echo rwmb_meta('projeto_Texto_Social_Media_Marketing')?></p>
					</article>							
				</div>						
			</div>
		</div>	
	</section>

	<?php 
		// TEMPLATE PROJETOS
		include (TEMPLATEPATH . '/templates/projeto/projeto.php');
	?>
	
	<!-- SESSÃO DEPOIMENTO CLIENTES -->
	<section class="hand-oqueFazemos-depoimentos">
		<div class="hand-container">
			<figure>
				<img src="<?php echo rwmb_meta('projeto_Mockup_depoimento')['full_url']?>" alt="<?php echo rwmb_meta('projeto_Mockup_depoimento')['full_url'];?>">
				<figcaption class="hidden"></figcaption>
			</figure>
		
			<div class="hand-texto-depoimento">					
				<p><?php echo rwmb_meta('projeto_depoimento_texto')?></p>
				<span class="hand-autor-depoimento"><?php echo rwmb_meta('projeto_depoimento_nome')?></span>
			</div>							
		</div>	
	</section>

	<!-- SESSÃO SERVIÇO DESIGN INTERFACE -->
	<section class="hand-oqueFazemos-servDesignInterface" >
		<div class="hand-container">
			<div class="row">						
				<div class="col-sm-6" style="display: none">
					<div class="hand-title-servico">
						<h3><?php echo rwmb_meta('projeto_Design_De_Interface');?></h3>
					</div>
					<article class="hand-descricao-servico hand-formatacao-texto">
						<p> <?php echo rwmb_meta('projeto_Texto_Design_De_Interface');?></p>
					</article>							
				</div>
				<div class="col-sm-12">
					<figure>
						<img src="<?php echo rwmb_meta('projeto_Mockup_Desktop')['full_url'];?>" alt="<?php echo rwmb_meta('projeto_Mockup_Desktop')['full_url'];?>">
						<figcaption class="hidden"><?php echo rwmb_meta('projeto_Mockup_Desktop')['full_url'];?></figcaption>
					</figure>
				</div>						
			</div>
		</div>	
	</section>

	<!-- SE HOUVER POST POSTERIOR MOSTRA A FOTO SE NÃO MOSTRA A FOTO DO PRIMEIRO POST DISPONÍVEL -->
	<?php $nextPost = get_next_post(); 
	
	if($nextPost) { 

		$nextPost = $nextPost;

	} else {

		$nextPost = get_previous_post();

	};

	$nextFoto = wp_get_attachment_image_src( get_post_thumbnail_id($nextPost->ID), 'full' );
	$nextFoto = $nextFoto[0];

	// var_dump($nextFoto);
	// var_dump($nextPost);
	// var_dump($nextPost->post_excerpt);
	// var_dump($nextPost->guid);
	// var_dump($nextPost->post_title);

	?>

	<section class="hand-proximoPost">
		<div class="hand-container">
			<article class="info-proximo-post">
				<div class="titulo-proximo-post">
					<span>Next project</span>
					<h3><?php echo $nextPost->post_title; ?></h3>
				</div>
				<div class="resumo-proximo-post">
					<p><?php echo $nextPost->post_excerpt; ?></p>
				</div>
			</article>
			<a href="<?php echo $nextPost->guid; ?>" class="imagem-proximo-post">
				<figure>
					<img src="<?php echo $nextFoto; ?>" alt="<?php echo $nextPost->post_title; ?>">
				</figure>
			</a>
		</div>
	</section>

</div>



<?php
get_footer(); 
