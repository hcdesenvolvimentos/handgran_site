<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projetos_handgran_site' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '123' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'toqn6e3n|WMc=ZJ.xLfup 8QiJpi&*Cv4{BjtM1sG^%`=yg/%_ ]{%m pBt;nR&.' );
define( 'SECURE_AUTH_KEY',  ']g|Wb_`%?.h-DJi`IKt!|2|?JUs@|*-Y*W `Ic5/AB7sK(=c$!9kR~3(9M*3=Hz=' );
define( 'LOGGED_IN_KEY',    'z3c3k/_mjLG($&-q9OmD~>ase4q0WLL.F(A%Idl@<wC@ouzNN,wpOdY-(|,,j3~v' );
define( 'NONCE_KEY',        'P:.sAji5$WuyX>>-CTbp|E//I[bm`WF}Zg3@TQSF3YIC|FO@T#i-$+QYiPj|H/1,' );
define( 'AUTH_SALT',        'vorM&;=>Ht_89Xq6.67i&)7K^b9EtH ub1gY+PF=RZy&OP|l%Oa#:SG$eoC>n(qq' );
define( 'SECURE_AUTH_SALT', 'VSE&tx{t!6$1K$I*: ]ShVB~!O_`;]>1~S.Nea:A@ld9c^nd(7VxQ>F,VIl~m~:@' );
define( 'LOGGED_IN_SALT',   'pb-5Pe7Qyu f2k{/W8K9mgOZfG`^uI*|S&<]teMmxw~Y|,#_c`8~,ROACq*,WUrb' );
define( 'NONCE_SALT',       'xx!S;E>#MwS~`UY$D=Lv((rv*3jvo<fGa!cmm&^-JCGtSU#%MrIR~9>2#{Sv4^-(' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'gr_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
define('WP_MEMORY_LIMIT', '128M');
define( 'ALLOW_UNFILTERED_UPLOADS', true );